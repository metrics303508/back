# Expenses App Backend

## How to run a dev server locally

1. Make sure you're using Node >= 18
1. Make sure you have a Mongo database you can connect to
    - A simple alternative if you have Docker installed on your machine, could be to use the official Mongo image running this command: `docker run -d -p 27017:27017 --name mongo-expenses -v mongo-expenses:/data/db mongo:latest`
        - The command will pull the image and run a container that exposes a Mongo instance on `mongodb://localhost:27017/`
1. Create a `.env` file following the format given in `.env.example`
    - Keep in mind that if you're connecting to the local Mongo instance created using the suggested approach, then the `DB_URL` should look like this: `"mongodb://127.0.0.1:27017/ANY_DB_NAME_YOU_LIKE"`
1. Run `npm run dev`
1. Test the API using your favorite client
    - If you'd like to use the companion frontend client, take a look at [this repo](https://gitlab.com/expenses303508/front/)
