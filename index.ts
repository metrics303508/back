import express from "express";
import cors from "cors";
import rateLimit from "express-rate-limit";
import * as dotenv from "dotenv";

import initDatabase from "./database";
import expensesRouter from "./expenses";

dotenv.config();
initDatabase();
const app = express();

app.use(
  cors({ origin: process.env.ALLOWED_ORIGIN, optionsSuccessStatus: 200 })
);
app.use(rateLimit({ windowMs: 60 * 1000, max: 120 }));
app.use(express.json());

app.use("/expenses", expensesRouter);

app.listen(process.env.PORT, () => {
  console.log(`Listening in port ${process.env.PORT}`);
});
