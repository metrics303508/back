import mongoose from "mongoose";

const expenseSchema = new mongoose.Schema({
  timestamp: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
});

expenseSchema.index({ timestamp: -1 });

export const Expense = mongoose.model("Expense", expenseSchema);
