import { Request, Response } from "express";

import { Expense } from "./models";

export const postHandler = async (req: Request, res: Response) => {
  try {
    const expense = new Expense({
      timestamp: req.body.timestamp,
      name: req.body.name,
      value: req.body.value,
    });

    const savedExpense = await expense.save();
    res.status(201).send(savedExpense);
  } catch (err) {
    res.status(400).send(err);
  }
};

export const getAllHandler = async (req: Request, res: Response) => {
  try {
    const results = await Expense.find().sort({ timestamp: -1 });
    res.send(results);
  } catch (err) {
    res.status(400).send(err);
  }
};

export const getHandler = async (req: Request, res: Response) => {
  try {
    const expense = await Expense.findById(req.params.id);

    if (!expense) {
      return res.status(404).send("Expense not found");
    }

    res.send(expense);
  } catch (err) {
    res.status(400).send(err);
  }
};

export const putHandler = async (req: Request, res: Response) => {
  try {
    const expense = await Expense.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    if (!expense) {
      return res.status(404);
    }

    res.send(expense);
  } catch (err) {
    res.status(400).send(err);
  }
};

export const deleteHandler = async (req: Request, res: Response) => {
  try {
    const expense = await Expense.findByIdAndRemove(req.params.id);

    if (!expense) {
      return res.status(404);
    }

    res.send(expense);
  } catch (err) {
    res.status(400).send(err);
  }
};
