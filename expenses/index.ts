import { Router } from "express";

import {
  deleteHandler,
  getAllHandler,
  getHandler,
  postHandler,
  putHandler,
} from "./handlers";

const router = Router();

router.post("/", postHandler);
router.get("/", getAllHandler);
router.get("/:id/", getHandler);
router.put("/:id/", putHandler);
router.delete("/:id/", deleteHandler);

export default router;
