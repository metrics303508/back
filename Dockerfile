FROM node:18
SHELL ["bash", "-c"]

# Create the app directory and copy the initial necessary files
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json .
COPY .env .

# Set the working directory
RUN cd /usr/src/app

# Load env vars
RUN source .env

# Install dependencies
RUN npm install

# Copy the rest of the project files
COPY . .

# Build the project
RUN npm run build

# Expose the app's port
EXPOSE $PORT

# Run the app
CMD ["npm", "start"]
