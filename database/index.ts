import mongoose from "mongoose";
import * as dotenv from "dotenv";

const init = () => {
  if (!process.env.ENV_INITIALIZED) {
    dotenv.config();
  }

  mongoose.connect(process.env.DB_URL as string);
};

export default init;
